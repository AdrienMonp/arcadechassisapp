var gulp = require('gulp');
var sass = require('gulp-sass');


//style paths
var sassFiles = 'src/index.scss',
    cssDest = 'src/css';

//nested, expanded, compact, compressed
gulp.task('sass', async function () {
    gulp.src(sassFiles)
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(gulp.dest(cssDest));
});

gulp.task('watch',function() {
    gulp.watch('src/**/*.scss',gulp.series('sass'));
});