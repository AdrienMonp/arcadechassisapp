// properties of an electrolytic capacitor
export const ECdataInfos = [
    {
        id: 'capacitance',
        length: 7
    },
    {
        id: 'voltage',
        length: 3
    },
    {
        id: 'bipolar',
        length: 1
    },
    {
        id: 'axial',
        length: 1
    },
    {
        id: 'snapIn',
        length: 1
    },
    {
        id: 'screw',
        length: 1
    },
    {
        id: 'SMD',
        length: 1
    }
];