import React, { useState, useEffect, useRef, useCallback, Fragment } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { ECdataInfos } from './componentDataInfos';
import { APIurl } from '../common/common';
import { openModale, closeModale, capaSpriteToCreate, setCapaList } from '../../actions';

const CapkitList = ({ chassisId, capkitPhoto }) => {

    // API url suffix to target electrolytics capacitors list
    const chassisUrl = 'chassis-items';

    // get info from global state
    const globalState = useSelector(state => state);
    const isLogged = globalState.login.logged;
    const APIkey = globalState.login.key;
    const globalCapaList = globalState.capaSprite.capaList;
    const capaCreationId = globalState.capaSprite.capaToCreate;

    // dispatcher init
    const dispatch = useDispatch();


    //-----------------------------------------//
    //---- DECODE CAPA ID TO GET ALL INFOS ----//
    //-----------------------------------------//

    const decodeCapaList = (list) => {
        let newList = [];

        // for each capa
        list.forEach((value, index) => {

            // push properties into a new object
            let newObj = { ...list[index] };
            let pos = 0;

            // split reference letter(s) from position
            newObj.reference = newObj.position.match(/^\D+/g)[0];
            newObj.position = parseInt(newObj.position.replace(/^\D+/g, ''));

            // remove EC prefix of capa id
            const splited = value.compId.replace(/^\D+/g, '');
            newObj.compId = splited;

            // split capa id with info from componentDataInfos and fill in the object
            ECdataInfos.forEach((item, index) => {
                let value = parseFloat(splited.slice(pos, item.length + pos));
                if (index === 0) (value = value / 100); // for capacitance case, get back the decimal number

                // set info into object
                newObj[item.id] = value;
                pos = pos + item.length;
            });

            // push the capa object into array
            newList.push(newObj);
        });

        return newList
    }


    //---------------------------------------------------//
    //---- HOOK STATES, EFFECTS AND REF DECLARATIONS ----//
    //---------------------------------------------------//

    // create state - newly created capa id
    const [stateCreatedCapaId, setStateCreatedCapaId] = useState('');

    // create state - new capa form visible or not
    const [stateNewCapaFromVisible, setstateNewCapaFromVisible] = useState(false);


    // ---------------------------- //
    // --- capa list management --- //

    // create state - list of capa
    const [stateCapaList, setStateCapaList] = useState([]);

    // when list of capa is repainted after capa creation, scroll to newly added capa
    useEffect(() => {
        if (itemToScrollTo.current && stateCreatedCapaId !== '') {
            itemToScrollTo.current.scrollIntoView({ behavior: "smooth" });
        }
    }, [stateCapaList, stateCreatedCapaId]);

    // --- end of capa list management --- //
    // ----------------------------------- //


    // --------------------------------- //
    // --- capa list save management --- //

    // create state - copy of initial list of capa for inserting new capa/displaying all capa style list
    const [stateCapaListSave, setStateCapaListSave] = useState([]);

    // when new capa inserted into stateCapaListSave, refresh list
    useEffect(() => {
        setStateCapaList([...stateCapaListSave]);
    }, [stateCapaListSave]);

    // --- end of capa list save management --- //
    // ---------------------------------------- //


    // create state - new capa values
    const [stateNewCapaPosition, setStateNewCapaPosition] = useState('');
    const [stateNewCapaCapacitance, setStateNewCapaCapacitance] = useState('');
    const [stateNewCapaVoltage, setStateNewCapaVoltage] = useState('');
    const [stateNewCapaBipolar, setStateNewCapaBipolar] = useState(0);
    const [stateNewCapaAxial, setStateNewCapaAxial] = useState(0);
    const [stateNewCapaSnapIn, setStateNewCapaSnapIn] = useState(0);
    const [stateNewCapaScrew, setStateNewCapaScrew] = useState(0);
    const [stateNewCapaSMD, setStateNewCapaSMD] = useState(0);
    const [stateNewCapaNotes, setStateNewCapaNotes] = useState('');


    // create state - total number of capa
    const [stateTotalNumberOfCapa, setStateTotalNumberOfCapa] = useState(0);

    // create state - type of sort
    const [stateSortType, setStateSortType] = useState('position');


    // ------------------------------------------- //
    // --- compressed/expanded list management --- //

    // create state
    const [stateCompressedList, setStateCompressedList] = useState(false);

    // useEffect switching between compressed and expanded list
    useEffect(() => {
        if (stateCompressedList) {
            displayCapkitList();
        } else {
            setStateSortType('position');
            setStateCapaList([...stateCapaListSave]);
        }
    }, [stateCompressedList]); // eslint-disable-line react-hooks/exhaustive-deps

    // --- end of compressed/expanded list management --- //
    // -------------------------------------------------- //


    // --------------------------------------------------- //
    // --- compressed/expanded list display management --- //

    // create state - list of sorted compressed capkit list
    const [stateCapkitList, setStateCapkitList] = useState([]);

    // when capkit compressed list is updated display it
    useEffect(() => {
        setStateCapaList([...stateCapkitList]);
    }, [stateCapkitList]);

    // --- end of compressed/expanded list display management --- //
    // ---------------------------------------------------------- //


    // create state - order of sorting
    const [stateSortAscOrder, setStateSortAscOrder] = useState(true);


    // clear capa sprite to create in global state
    useEffect(() => {
        dispatch(capaSpriteToCreate(null));
    }, [dispatch])


    //-------------------------------//
    //---- REFERENCE DECLARTIONS ----//
    //-------------------------------//

    // ref used to scroll to a newly adde capa
    const itemToScrollTo = useRef(null);

    // target position input to focus it
    const positionInput = useRef(null);


    //-------------------------------------------//
    //---- TREAT CAPA LIST FROM GLOBAL STATE ----//
    //-------------------------------------------//

    const capaListTreatments = useCallback(() => {
        let list = [...globalCapaList];
        // decode capa API data
        list = decodeCapaList(list);
        // sort them
        list = sortFunction(list, 'position');
        // set total number of capa
        setStateTotalNumberOfCapa(list.length);
        // update state list (and repaint thanks to useEffect)
        setStateCapaListSave(list);
    }, [globalCapaList]);

    // when global capa list changes, update local lists
    useEffect(() => {
        capaListTreatments();
    }, [globalCapaList, capaListTreatments]);


    //-----------------------------//
    //---- CAPA SORT FUNCTIONS ----//
    //-----------------------------//

    // generic sort function (insertion type)
    const sortFunction = (list, key, asc = true) => {
        // sort on key
        let newList = [...list];
        let j;
        for (let i = 1; i < newList.length; i++) {
            let tmp = newList[i];
            if (asc) { // ascending sort
                for (j = i - 1; j >= 0 && (newList[j][key] > tmp[key]); j--) {
                    newList[j + 1] = newList[j];
                }
            } else { // descending sort
                for (j = i - 1; j >= 0 && (newList[j][key] < tmp[key]); j--) {
                    newList[j + 1] = newList[j];
                }
            }
            newList[j + 1] = tmp;
        }
        return newList;
    }

    // split a table into sub arrays with same key value
    const splitTableOnSameKey = (list, key) => {
        let listByValueTable = [];
        let start = 0;
        let prevVal = list[0][key]
        for (let i = 0; i < list.length; i++) {
            if (i + 1 >= list.length || list[i + 1][key] !== prevVal) {
                listByValueTable.push(list.slice(start, i + 1));
                start = i + 1;
            }
            prevVal = i + 1 < list.length ? list[i + 1][key] : '';
        }
        return listByValueTable;
    }


    // set state sort type and call sort function
    const capaListSort = (key1, key2 = null) => {
        setStateSortType(key1);
        setStateCreatedCapaId(''); // reset state created id to avoid style and scroll to previously created capa
        capaSort(stateCapaList, stateSortAscOrder, key1, key2);
    };


    // capa table sort function
    const capaSort = (list, asc, key1, key2 = null) => {

        // sort on the first key
        let newList = sortFunction([...list], key1, asc);

        // if a second key to sort exists
        if (key2 != null) {

            // splice into sub arrays with same key1
            const listByValueTable = splitTableOnSameKey([...newList], key1);

            // sort those sub arrays on 2nd key
            newList = [];
            listByValueTable.forEach((value) => {
                if (value.length > 1) {
                    // sort only if more than 1 item
                    const sortedSubArray = sortFunction(value, key2, asc);
                    newList = [...newList, ...sortedSubArray];
                } else {
                    // else push directly
                    newList.push(value[0]);
                }
            });
        }

        // update state at the end
        setStateCapaList(newList);
    }


    // compressed capkit sort function
    const displayCapkitList = () => {

        // set state sort type to compId
        setStateSortType('compId');

        // sort on type
        let newList = sortFunction([...stateCapaList], 'compId', true);

        let capkitList = [];
        let prevType = '';
        let provObj = {};
        let count = 1;

        // fetch all capas to get a new array with only different types of capa
        newList.forEach((item, key) => {
            if (item.compId === prevType) {
                count++;
                provObj.number = count;
                provObj.position.push(item.reference + item.position);
                provObj.id.push(item.id);
            } else {
                if (key) { capkitList.push(provObj) }; // dont push empty obj on first loop
                provObj = { ...item };
                prevType = item.compId;
                count = 1;
                provObj.number = count;
                provObj.position = [item.reference + item.position];
                provObj.id = [item.id];
            }
        });
        capkitList.push(provObj); // push remaining obj on last loop

        // Update state to repaint list
        setStateCapkitList(capkitList);
    };


    //------------------------------//
    //---- DELETE CAPA FUNCTION ----//
    //------------------------------//

    // open modale
    const deleteCapaModale = (e, positions, capacitance, voltage, bipolar, axial, snapIn, screw, SMD) => {
        const id = e.currentTarget.attributes['data-id'].value;

        return (
            <div className="modaleDeleteCapa">
                Are you sure to delete capacitor {positions} - {capacitance}μF - {voltage}v {bipolar} {axial} {snapIn} {screw} {SMD} ?
                <div className="modaleDeleteCapaBtnContainer">
                    <button onClick={() => deleteCapa(id)}>Yes</button>
                </div>
            </div>
        )
    }

    // delete capa function
    const deleteCapa = (id) => {

        // send id to delete to API
        fetch(`${APIurl}/${chassisUrl}/deletecapa/${chassisId}/${id}`, {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json; charset=UTF-8', 'Authorization': `Bearer ${APIkey}` }
        })
            .then(result => result.json())
            .then(json => {
                if (typeof json.error !== 'undefined') { // error catch
                    console.log(`Error ${json.statusCode} : ${json.error} - ${json.message}`);
                } else {
                    // close modale
                    dispatch(closeModale());

                    // find capa to delete and update global capa list without it
                    let newGlobalCapaList = [...globalCapaList];
                    const indexOfCapa = globalCapaList.findIndex(i => i.id === parseInt(id));
                    if (indexOfCapa > -1) {
                        newGlobalCapaList.splice(indexOfCapa, 1);
                    }
                    // set global capa list
                    dispatch(setCapaList(newGlobalCapaList));
                }
            });
    };


    //--------------------------------//
    //---- ADD NEW CAPA FUNCTIONS ----//
    //--------------------------------//

    const AddCapa = (e) => {
        e.preventDefault();

        // transform capacitance number to a 7 digits string without '.'
        let capacitance = String(Math.floor((parseFloat(stateNewCapaCapacitance).toFixed(2)) * 100)); // add 2 decimals and remove '.'
        let capaLength = ECdataInfos[0].length; // get length from ECdataInfos
        while (capacitance.length < capaLength) { // add '0's to the begining
            capacitance = "0" + capacitance;
        }

        // transform voltage into a 3 digits string
        let voltage = String(stateNewCapaVoltage);
        let voltLength = ECdataInfos[1].length; // get length from ECdataInfos
        while (voltage.length < voltLength) { // add '0's to the begining
            voltage = "0" + voltage;
        }

        // set other infos
        const bipolar = stateNewCapaBipolar,
            axial = stateNewCapaAxial,
            snapIn = stateNewCapaSnapIn,
            screw = stateNewCapaScrew,
            SMD = stateNewCapaSMD;

        const fullPosition = 'C' + stateNewCapaPosition;
        const compId = capacitance + voltage + bipolar + axial + snapIn + screw + SMD;

        // send new object to API
        fetch(`${APIurl}/${chassisUrl}/addcapa/${chassisId}`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json; charset=UTF-8', 'Authorization': `Bearer ${APIkey}` },
            body: JSON.stringify({
                position: fullPosition,
                compId: 'EC' + compId,
                notes: stateNewCapaNotes !== '' ? stateNewCapaNotes : null
            })
        })
            .then(result => result.json())
            .then(json => {
                if (typeof json.error !== 'undefined') { // error catch
                    console.log(`Error ${json.statusCode} : ${json.error} - ${json.message}`);
                } else { // once item created in API, insert it into the global list

                    // memorize id to style it in the new list
                    setStateCreatedCapaId(json.id);


                    // reset new capa states, inputs, sort order
                    setStateNewCapaPosition('');
                    setStateNewCapaCapacitance('');
                    setStateNewCapaVoltage('');
                    setStateNewCapaBipolar(0);
                    setStateNewCapaAxial(0);
                    setStateNewCapaSnapIn(0);
                    setStateNewCapaScrew(0);
                    setStateNewCapaSMD(0);
                    setStateNewCapaNotes('');
                    setStateSortAscOrder(true);

                    // set global capa list
                    dispatch(setCapaList([...globalCapaList, json]));

                    // focus first input for next adding
                    positionInput.current.focus();
                }
            });
    }


    //----------------------------------------------//
    //---- CAPA CANVAS COMMUNICATION FUNCTIONS  ----//
    //----------------------------------------------//

    // set global state with capa sprite to create
    const createCapaSprite = (id) => {
        dispatch(capaSpriteToCreate(id));
    }

    // hover to show capa on canvas
    const showCapaMouseEnter = (ids) => {
      
        // multi capa hover management
        if (typeof ids === 'number') {
            const elt = document.getElementById('capaSprite_' + ids);
            if (elt) {
                elt.classList.add('selected');
            }
        } else {
            ids.forEach((item, key) => {
                const elt = document.getElementById('capaSprite_' + item);
                if (elt) {
                    elt.classList.add('selected');
                }
            });
        }
    }
    // end of hovering a capa
    const showCapaMouseLeave = (ids) => {

        // multi capa hover management
        if (typeof ids === 'number') {
            const elt = document.getElementById('capaSprite_' + ids);
            if (elt) {
                elt.classList.remove('selected');
            }
        } else {
            ids.forEach((item) => {
                const elt = document.getElementById('capaSprite_' + item);
                if (elt) {
                    elt.classList.remove('selected');
                }
            });
        }
    }


    //---------------------------------------------//
    //---- NEW CAPA STATE CHANGES WITH INPUTS  ----//
    //---------------------------------------------//

    const changeNewCapaPosition = (e) => {
        setStateNewCapaPosition(e.target.value);
    }
    const changeNewCapaCapacitance = (e) => {
        setStateNewCapaCapacitance(e.target.value);
    }
    const changeNewCapaVoltage = (e) => {
        setStateNewCapaVoltage(e.target.value);
    }
    const changeNewCapaBipolar = (e) => {
        setStateNewCapaBipolar(e.target.checked ? 1 : 0);
    }
    const changeNewCapaAxial = (e) => {
        setStateNewCapaAxial(e.target.checked ? 1 : 0);
    }
    const changeNewCapaSnapIn = (e) => {
        setStateNewCapaSnapIn(e.target.checked ? 1 : 0);
    }
    const changeNewCapaScrew = (e) => {
        setStateNewCapaScrew(e.target.checked ? 1 : 0);
    }
    const changeNewCapaSMD = (e) => {
        setStateNewCapaSMD(e.target.checked ? 1 : 0);
    }
    const changeNewCapaNotes = (e) => {
        setStateNewCapaNotes(e.target.value);
    }


    //-------------------------------------//
    //---- RENDER ADD NEW CAPA INPUTS  ----//
    //-------------------------------------//

    const renderAddCapaInput = () => {

        if (isLogged) {
            // check if form is open
            let isVisible = ''
            if (stateNewCapaFromVisible) {
                isVisible = ' isVisible';
            }

            return (
                <form className={'capaFormContainer' + isVisible} onSubmit={(e) => AddCapa(e)}>
                    <label className="label__1" htmlFor="capaPositionInput">Position</label>
                    <label className="label__2" htmlFor="capaCapacitanceInput">Capacitance (μF)</label>
                    <label className="label__3" htmlFor="capaVoltageInput">Voltage (v)</label>
                    <label className="label__4" htmlFor="capaBipolarInput">Bipolar</label>
                    <label className="label__5" htmlFor="capaAxialInput">Axial</label>
                    <label className="label__6" htmlFor="capaSnapInInput">Snap in</label>
                    <label className="label__7" htmlFor="capaScrewInput">Screw terminal</label>
                    <label className="label__8" htmlFor="capaSMDInput">SMD</label>

                    <input className="input__1" type="number" id="capaPositionInput" value={stateNewCapaPosition} onChange={(e) => changeNewCapaPosition(e)} ref={positionInput} min="0" max="999" required />
                    <input className="input__2" type="number" id="capaCapacitanceInput" value={stateNewCapaCapacitance} onChange={(e) => changeNewCapaCapacitance(e)} step="0.01" min="0" max="99999" required />
                    <input className="input__3" type="number" id="capaVoltageInput" value={stateNewCapaVoltage} onChange={(e) => changeNewCapaVoltage(e)} min="0" max="999" required />
                    <input className="input__4" type="checkbox" id="capaBipolarInput" onChange={(e) => changeNewCapaBipolar(e)} checked={stateNewCapaBipolar} />
                    <input className="input__5" type="checkbox" id="capaAxialInput" onChange={(e) => changeNewCapaAxial(e)} checked={stateNewCapaAxial} />
                    <input className="input__6" type="checkbox" id="capaSnapInInput" onChange={(e) => changeNewCapaSnapIn(e)} checked={stateNewCapaSnapIn} />
                    <input className="input__7" type="checkbox" id="capaScrewInput" onChange={(e) => changeNewCapaScrew(e)} checked={stateNewCapaScrew} />
                    <input className="input__8" type="checkbox" id="capaSMDInput" onChange={(e) => changeNewCapaSMD(e)} checked={stateNewCapaSMD} />

                    <input type="submit" value="Add" />

                    <textarea className="textarea__1" placeholder="Notes" value={stateNewCapaNotes} onChange={(e) => changeNewCapaNotes(e)}></textarea>
                </form>
            );
        } else {
            return null;
        }
    }


    //------------------------------//
    //---- RENDER CAPKIT BUTTON ----//
    //------------------------------//

    const renderCapkitButton = () => {
        // change button text
        const text = stateCompressedList ? 'Show all components list' : 'Show compressed components list';

        let showCreationFormBtn = ''
        if (isLogged) {
            const isOpen = stateNewCapaFromVisible ? ' exitMode' : '';
            const btnText = stateNewCapaFromVisible ? 'Close form' : 'Add new capa';
            showCreationFormBtn = <button className={'capaListShowAddBtn' + isOpen} onClick={(e) => { setstateNewCapaFromVisible(!stateNewCapaFromVisible) }}>{btnText}</button>
        }

        return (
            <div className="capaListTopBlock">
                <button className="capaListCapkitBtn" onClick={() => { setStateCompressedList(!stateCompressedList) }}>{text}</button>
                <div className="capaListTotalNumber">Total number of capacitors : {stateTotalNumberOfCapa}</div>
                <div className="capaListShowAddBtnContainer">{showCreationFormBtn}</div>
            </div>
        );
    }

    //----------------------------------//
    //---- RENDER CAPA SORT BUTTONS ----//
    //----------------------------------//

    const renderCapaSortButtons = () => {
        // change sort type for position when compressed capkit list is displayed
        let positionSort = stateCompressedList ? 'compId' : 'position';

        return (
            <div className={'capaSortBtnContainer'}>
                <div className="capaSortBtnItem">
                    <button onClick={() => capaListSort(positionSort)}>Position</button>
                </div>
                <div className="capaSortBtnItem">
                    <button onClick={() => capaListSort('capacitance', 'voltage')}>Capacitance</button>
                </div>
                <div className="capaSortBtnItem">
                    <button onClick={() => capaListSort('voltage', 'capacitance')}>Voltage</button>
                </div>
                <div className="capaSortBtnItem">Special</div>
                <div className="capaSortBtnItem">Notes</div>
            </div>
        );
    }


    //---------------------------//
    //---- RENDER CAPA LIST ----//
    //--------------------------//

    const RenderCapaList = () => {

        const type = stateSortType;
        let prevValue = stateCapaList.length ? stateCapaList[0][type] : '';
        let classSuffix = 'odd';

        return stateCapaList.map((value, index) => {
            let addedItem = '';

            // extra infos
            let bipolar = value.bipolar ? <strong>bipolar</strong> : '';
            let axial = value.axial ? <strong>axial</strong> : '';
            let snapIn = value.snapIn ? <strong>snap in</strong> : '';
            let screw = value.screw ? <strong>screw terminal</strong> : '';
            let SMD = value.SMD ? <strong>SMD</strong> : '';

            // check previous value to style different types
            if (value[type] !== prevValue) {
                classSuffix = classSuffix === 'odd' ? 'even' : 'odd';
            }
            prevValue = value[type];

            // if newly added capa, set ref for scrolling and class for style
            let opts = {};
            if (value.id === stateCreatedCapaId) {
                opts['ref'] = itemToScrollTo;
                addedItem = ' addedItem';
            }

            // multiple position when in compressed capkit list
            let positions = '';
            if (typeof value.position === 'number') {
                positions = value.reference + value.position
            } else {
                positions = value.position.map((item, key) => {
                    return <span key={key}>{item}</span>;
                });
            }

            // disable delete button when compressed capkit list is displayed
            let disabled = stateCompressedList ? true : false;

            // delete button
            let deleteBtn = isLogged ? <button className="capaListSupprBtn" data-id={value.id} onClick={(e) => dispatch(openModale(deleteCapaModale(e, positions, value.capacitance, value.voltage, bipolar, axial, snapIn, screw, SMD)))} disabled={disabled}>
                <img className="capaListSupprIcon" src={`${process.env.PUBLIC_URL}/images/icons/delete.svg`} alt="delete" />
            </button> : '';

            // add sprite button (only if logged and expanded list)
            let addSpriteBtn = '';
            if (isLogged && !disabled) {
                const isDisabled = value.id === capaCreationId ? true : false;
                addSpriteBtn = capkitPhoto !== null && value.sprite === null ? <button className="capaListAddSpriteBtn" onClick={() => createCapaSprite(value.id)} disabled={isDisabled}>add<br />sprite</button> : '';
            }

            // notes
            const notes = value.notes ? <Fragment><div className="capaNoteSign">note</div><div className="capaNote">{value.notes.split('\n').map((item, key) => <div key={key}>{item}</div>)}</div></Fragment> : ''

            return (
                <li key={index} {...opts} className={'rowTable__' + classSuffix + addedItem} onMouseEnter={() => showCapaMouseEnter(value.id)} onMouseLeave={() => showCapaMouseLeave(value.id)}>
                    {addSpriteBtn}
                    <div className='capaListPositionCol'>{positions}</div>
                    <div>{value.capacitance}μF</div>
                    <div>{value.voltage}v</div>
                    <div>{bipolar} {axial} {snapIn} {screw} {SMD}</div>
                    <div>{notes}</div>
                    {deleteBtn}
                </li>
            );
        });
    }


    //---------------------//
    //---- MAIN RENDER ----//
    //---------------------//

    return (
        <div className="capkitListContainer">
            {renderAddCapaInput()}
            {renderCapkitButton()}
            {renderCapaSortButtons()}
            <div className="capaListContainer">
                <ul className="capaList">
                    {RenderCapaList()}
                </ul>
            </div>
        </div>
    )
}

export default CapkitList;
