import React, { useState, useEffect, Fragment } from 'react'
import { useSelector } from 'react-redux';
import { APIurl } from '../common/common';

const ChassisInfos = ({ chassisId, chassisInfos }) => {

    // API url suffix to target infos list
    const chassisUrl = 'chassis-items';

    // get logged info from global state
    const globalState = useSelector(state => state);
    const isLogged = globalState.login.logged;


    //---------------------------------------------------//
    //---- HOOK STATES, EFFECTS AND REF DECLARATIONS ----//
    //---------------------------------------------------//

    // set state - id saved to edit object
    const [stateObjectId, setstateObjectId] = useState(null);

    // set state - infos values
    const [stateValues, setstateValues] = useState({});

    // set state - copy of infos value to edit them
    const [stateEditedValues, setstateEditedValues] = useState({});

    // set state - edit mode on / off
    const [stateEditMode, setstateEditMode] = useState(false);

    // when chassisInfos paramater change, re-render
    useEffect(() => {
        // set infos values into state
        setstateValues({ ...chassisInfos });
        // save id into state to update later
        setstateObjectId(chassisInfos.url);
    }, [chassisInfos]);


    // switch between edit mode
    const switchEditMode = () => {
        if (!stateEditMode) { // if entering edit mode, copy infos into editable state
            setstateEditedValues(stateValues);
        }
        // switch state edit mode
        let mode = !stateEditMode;
        setstateEditMode(mode);
    }


    //-----------------------------------//
    //---- SEND UPDATED INFOS TO API ----//
    //-----------------------------------//

    const submitForm = (e) => {
        e.preventDefault();

        // send infos to update to API
        fetch(`${APIurl}/${chassisUrl}/${stateObjectId}`, {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json; charset=UTF-8', 'Authorization': `Bearer ${globalState.login.key}` },
            body: JSON.stringify({ ...stateEditedValues })
        })
            .then(result => result.json())
            .then(json => {
                if (typeof json.error !== 'undefined') { // error catch
                    console.log(`Error ${json.statusCode} : ${json.error} - ${json.message}`);
                } else {
                    // switch back to normal mode
                    setstateEditMode(false);
                    // update state with new values
                    setstateValues({ ...stateEditedValues })
                }
            });
    }


    //------------------------------------//
    //---- INPUT CHANGE UPDATES STATE ----//
    //------------------------------------//

    const changeValue = (e, prop, value = 'value', opposite = false) => {
        let values = { ...stateEditedValues };
        values[prop] = opposite ? !e.target[value] : e.target[value]; // check opposite param for radio btn case
        setstateEditedValues(values);
    }


    //--------------------------------------------//
    //---- ITEMS FORM INPUTS RENDER FUNCTIONS ----//
    //--------------------------------------------//

    const renderGeneralInfosForm = () => {

        return (
            <ul className="chassisInfoList">
                <li>
                    <div><label htmlFor="infosRef">Reference</label></div>
                    <div><input type="text" id="infosRef" value={stateEditedValues.ref ? stateEditedValues.ref : ''} onChange={(e) => changeValue(e, 'ref')} /></div>
                </li>
                <li>
                    <div><label htmlFor="infosBrand">Brand</label></div>
                    <div><input type="text" id="infosBrand" defaultValue={stateEditedValues.manufacturer.name ? stateEditedValues.manufacturer.name : ''} disabled /></div>
                </li>
                <li>
                    <div><label htmlFor="infosYear">Year</label></div>
                    <div><input type="text" id="infosYear" value={stateEditedValues.year ? stateEditedValues.year : ''} onChange={(e) => changeValue(e, 'year')} /></div>
                </li>
                <li>
                    <div>Video</div>
                    <div>
                        <label htmlFor="infosScreenSize">screen size</label>
                        <input type="number" id="infosScreenSize" value={stateEditedValues.screenSize ? stateEditedValues.screenSize : ''} onChange={(e) => changeValue(e, 'screenSize')} />
                        <label htmlFor="infosRaster">raster</label>
                        <input type="radio" name="rasterInput" id="infosRaster" checked={stateEditedValues.raster} onChange={(e) => changeValue(e, 'raster', 'checked')} />
                        <label htmlFor="infosVecto"> / vector</label>
                        <input type="radio" name="rasterInput" id="infosVecto" checked={!stateEditedValues.raster} onChange={(e) => changeValue(e, 'raster', 'checked', true)} />
                        <label htmlFor="infosColor"> | color</label>
                        <input type="radio" name="colorInput" id="infosColor" checked={stateEditedValues.colorDisplay} onChange={(e) => changeValue(e, 'colorDisplay', 'checked')} />
                        <label htmlFor="infosBaW"> / black &amp; white</label>
                        <input type="radio" name="colorInput" id="infosBaW" checked={!stateEditedValues.colorDisplay} onChange={(e) => changeValue(e, 'colorDisplay', 'checked', true)} />
                        <label htmlFor="infos15Khz"> | 15khz</label>
                        <input type="checkbox" id="infos15Khz" checked={stateEditedValues.freqFifteenKhz} onChange={(e) => changeValue(e, 'freqFifteenKhz', 'checked')} />
                        <label htmlFor="infos24Khz"> / 24khz</label>
                        <input type="checkbox" id="infos24Khz" checked={stateEditedValues.freqTwentyFourKhz} onChange={(e) => changeValue(e, 'freqTwentyFourKhz', 'checked')} />
                        <label htmlFor="infos31Khz"> / 31khz</label>
                        <input type="checkbox" id="infos31Khz" checked={stateEditedValues.freqThirtyOneKhz} onChange={(e) => changeValue(e, 'freqThirtyOneKhz', 'checked')} />
                        <label htmlFor="infosAutoSwitch"> | auto switch</label>
                        <input type="checkbox" id="infosAutoSwitch" checked={stateEditedValues.freqAutoSwitch} onChange={(e) => changeValue(e, 'freqAutoSwitch', 'checked')} />
                    </div>
                </li>
                <li>
                    <div>Synchro</div>
                    <div>
                        <label htmlFor="infosSynchCompo">composite</label>
                        <input type="checkbox" id="infosSynchCompo" checked={stateEditedValues.synchComposite} onChange={(e) => changeValue(e, 'synchComposite', 'checked')} />
                        <label htmlFor="infosSynchSep"> / separated</label>
                        <input type="checkbox" id="infosSynchSep" checked={stateEditedValues.synchSeparated} onChange={(e) => changeValue(e, 'synchSeparated', 'checked')} />
                        <label htmlFor="infosSynchNeg"> | negative</label>
                        <input type="checkbox" id="infosSynchNegp" checked={stateEditedValues.synchNegative} onChange={(e) => changeValue(e, 'synchNegative', 'checked')} />
                        <label htmlFor="infosSynchPos"> / positive</label>
                        <input type="checkbox" id="infosSynchPos" checked={stateEditedValues.synchPositive} onChange={(e) => changeValue(e, 'synchPositive', 'checked')} />
                    </div>
                </li>
                <li>
                    <div>Power input</div>
                    <div>
                        <label htmlFor="infosInputVoltage">power input voltage (VAC)</label>
                        <input type="number" id="infosInputVoltage" value={stateEditedValues.powerInputVoltage ? stateEditedValues.powerInputVoltage : ''} onChange={(e) => changeValue(e, 'powerInputVoltage')} />
                        <label htmlFor="infosIsolation"> | isolation transformer required</label>
                        <input type="checkbox" id="infosIsolation" checked={stateEditedValues.isolationRequired} onChange={(e) => changeValue(e, 'isolationRequired', 'checked')} />
                    </div>
                </li>
                <li>
                    <div>Yoke impedance</div>
                    <div>
                        <div>
                            <label htmlFor="infosImpHoriz">horizontal (ohm)</label>
                            <input type="number" id="infosImpHoriz" value={stateEditedValues.impedanceHorizontal ? stateEditedValues.impedanceHorizontal : ''} onChange={(e) => changeValue(e, 'impedanceHorizontal')} />
                        </div>
                        <div>
                            <label htmlFor="infosImpVert">vertical (ohm)</label>
                            <input type="number" id="infosImpVert" value={stateEditedValues.impedanceVertical ? stateEditedValues.impedanceVertical : ''} onChange={(e) => changeValue(e, 'impedanceVertical')} />
                        </div>
                    </div>
                </li>
                <li>
                    <div><label htmlFor="infosMonitor">Compatible monitors</label></div>
                    <div>
                        <textarea id="infosMonitor" rows="5" value={stateEditedValues.compatibleMonitor ? stateEditedValues.compatibleMonitor : ''} onChange={(e) => changeValue(e, 'compatibleMonitor')}></textarea>
                    </div>
                </li>
                <li>
                    <div><label htmlFor="infosCabinets">Equiped cabinets</label></div>
                    <div>
                        <textarea id="infosCabinets" rows="5" value={stateEditedValues.cabinet ? stateEditedValues.cabinet : ''} onChange={(e) => changeValue(e, 'cabinet')}></textarea>
                    </div>
                </li>
            </ul>
        );
    }


    //--------------------------------//
    //---- ITEMS RENDER FUNCTIONS ----//
    //--------------------------------//

    const renderGeneralInfos = () => {

        // video line
        let screenSize = stateValues.screenSize ? <span>{stateValues.screenSize} inches</span> : '';
        let raster = stateValues.raster ? 'raster' : 'vector';
        let color = stateValues.colorDisplay ? 'color' : 'black & white';
        let lowFreq = stateValues.freqFifteenKhz ? <span>15</span> : '';
        let freqSep1 = stateValues.freqFifteenKhz && stateValues.freqTwentyFourKhz ? '-' : '';
        let mediumFreq = stateValues.freqTwentyFourKhz ? <span>24</span> : '';
        let freqSep2 = stateValues.freqTwentyFourKhz && stateValues.freqThirtyOneKhz ? '-' : '';
        let highFreq = stateValues.freqThirtyOneKhz ? <span>31</span> : '';
        let freqUnit = stateValues.freqFifteenKhz || stateValues.freqTwentyFourKhz || stateValues.freqThirtyOneKhz ? 'khz' : '';
        let autoSwitch = stateValues.freqAutoSwitch ? <span>auto switch</span> : '';


        // synchro line
        let composite = stateValues.synchComposite ? <span>composite</span> : '';
        let separated = stateValues.synchSeparated ? <span>separated</span> : '';
        let negative = stateValues.synchNegative ? <span>negative</span> : '';
        let positive = stateValues.synchPositive ? <span>positive</span> : '';

        // power input line
        let inputVoltage = stateValues.powerInputVoltage ? <span>{stateValues.powerInputVoltage} VAC</span> : '';
        let isolation = stateValues.isolationRequired ? <span>(isolation transformer required)</span> : '';

        // yoke impedance line
        let impHoriz = stateValues.impedanceHorizontal ? <div>horizontal: {stateValues.impedanceHorizontal}Ω</div> : '';
        let impVert = stateValues.impedanceVertical ? <div>vertical: {stateValues.impedanceVertical}Ω</div> : '';

        // monitors line (add line breaks)
        const monitors = stateValues.compatibleMonitor ? stateValues.compatibleMonitor.split('\n').map((item, key) => <div key={key}>{item}</div>) : '';

        // cabinets line (add line breaks)
        const cabinets = stateValues.cabinet ? stateValues.cabinet.split('\n').map((item, key) => <div key={key}>{item}</div>) : '';

        return (
            <ul className="chassisInfoList">
                <li><span>Reference</span> <span>{stateValues.ref}</span></li>
                <li><span>Brand</span> <span>{stateValues.manufacturer.name}</span></li>
                <li><span>Year</span> <span>{stateValues.year}</span></li>
                <li><span>Video</span> <span>{screenSize} {raster} {color} {lowFreq}{freqSep1}{mediumFreq}{freqSep2}{highFreq}{freqUnit} {autoSwitch}</span></li>
                <li><span>Synchro</span> <span>{composite} {separated} {negative} {positive}</span></li>
                <li><span>Power input</span> <span>{inputVoltage} {isolation}</span></li>
                <li><span>Yoke impedance</span> <div>{impHoriz} {impVert}</div></li>
                <li><div>Default monitor</div> <div>{monitors}</div></li>
                <li><div>Equiped cabinets</div> <div>{cabinets}</div></li>
            </ul>
        )
    }


    //----------------------//
    //---- NOTES RENDER ----//
    //----------------------//

    const renderNotes = () => {
        const notes = stateValues.notes;
        if (notes.length) {
            // get notes
            const notesList = notes.map((item, key) => {
                return <li key={key}>{item.text}</li>
            });
            return (
                <div className="notesContainer">
                    <h2>Notes</h2>
                    <ul className="notesList">
                        {notesList}
                    </ul>
                </div>
            )
        } else {
            return null;
        }
    }


    //--------------------------//
    //---- DOCUMENTS RENDER ----//
    //--------------------------//

    const renderDocuments = () => {
        const documents = chassisInfos.documents;
        if (documents.length) {
            // get docs
            const docList = documents.map((item, key) => {
                return <a href={APIurl + item.document.url} className="documentLink" key={key} target="_blank" rel="noopener noreferrer"><img src={`${process.env.PUBLIC_URL}/images/icons/pdf-file.svg`} alt="pdf" /><span>{item.name}</span></a>
            });
            return (
                <Fragment>
                    <h2>Documents</h2>
                    {docList}
                </Fragment>
            )
        } else {
            return null
        }
    }


    //---------------------//
    //---- MAIN RENDER ----//
    //---------------------//

    if (Object.keys(stateValues).length !== 0) { // if datas are empty don't display anything

        if (!stateEditMode) { // display infos
            let editButton = isLogged ? <button className="chassisInfosEditButton" onClick={() => switchEditMode()}>Edit</button> : '';

            return (
                <div className="chassisInfosContainer">
                    <h2 className="infosTitle">Informations</h2>
                    {editButton}
                    {renderGeneralInfos()}
                    {renderNotes()}
                    {renderDocuments()}
                </div>
            )
        } else { // display edit form
            return (
                <div className="chassisInfosContainer">
                    <h2 className="infosTitle">Informations</h2>
                    <button className="chassisInfosEditButton exitMode" onClick={() => switchEditMode()}>Cancel</button>
                    <form onSubmit={(e) => submitForm(e)}>
                        {renderGeneralInfosForm()}
                        <div className="infoSubmitBtnContainer">
                            <input className="infoSubmitBtn" type="submit" value="Sauvegarder" />
                        </div>
                    </form>
                </div>
            )
        }
    } else {
        return null;
    }
}

export default ChassisInfos
