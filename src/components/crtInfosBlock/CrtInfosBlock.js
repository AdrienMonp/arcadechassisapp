import React, { Fragment, useState, useEffect, useCallback } from 'react';
import { APIurl } from '../common/common';

const CrtInfosBlock = ({ crtInfos }) => {

    //---------------------------------------------------//
    //---- HOOK STATES, EFFECTS AND REF DECLARATIONS ----//
    //---------------------------------------------------//

    // state list of compatible monitors
    const [stateMonitorList, setstateMonitorList] = useState([]);

    // state list open or not
    const [stateMonitorListOpen, setstateMonitorListOpen] = useState(false)

    // memoize tubes list from API
    const getTubesFromAPI = useCallback(async (size, heater, G1) => {

        await fetch(`${APIurl}/monitors/${size}`)
            .then(result => result.json())
            .then(json => {
                if (typeof json.error !== 'undefined') { // error catch
                    console.log(`Error ${json.statusCode} : ${json.error} - ${json.message}`);
                } else {
                    console.log(json);
                    const compatibleTubes = [];
                    const tubes = json.tubes;
                    tubes.forEach((item, key) => {
                        if (item.heater === heater && item.G1 === G1) {
                            compatibleTubes.push(item);
                        }
                    });
                    if (compatibleTubes.length) {
                        setstateMonitorList(compatibleTubes);
                    }
                }
            });
    }, []);

    // when crt infos available, get compatible tube from api
    useEffect(() => {
        // check if needed values exist
        if (crtInfos && crtInfos.screenSize && crtInfos.heaterVoltage && crtInfos.G1) {
            getTubesFromAPI(crtInfos.screenSize, crtInfos.heaterVoltage, crtInfos.G1);
        }
    }, [crtInfos, getTubesFromAPI]);



    //---------------------//
    //---- RENDER LIST ----//
    //---------------------//

    const renderList = () => {

        let crtSocket = '';
        if (crtInfos.crtSocket) {
            const photo = crtInfos.crtSocket.photo ? <span><a className="linkStyle" href={APIurl + crtInfos.crtSocket.photo.url} target="_blank" rel="noopener noreferrer">{crtInfos.crtSocket.ref}</a></span> : '';
            const reference = photo !== '' ? <li><span>Socket</span>{photo}</li> : <li><span>Socket</span><span>{crtInfos.crtSocket.ref} {photo}</span></li>;
            const datasheet = crtInfos.crtSocket.datasheet ? <li><span>Rejuvenator<br />adapter</span><span><a className="linkStyle" href={APIurl + crtInfos.crtSocket.datasheet.url} target="_blank" rel="noopener noreferrer">datasheet</a></span></li> : '';
            const pinouts = crtInfos.crtSocket.pinouts.map((item, key) => {
                return (
                    <li key={key}>
                        <span>{item.pin}</span>
                        <span>{item.role}</span>
                    </li>
                )
            });
            crtSocket = <Fragment>
                {reference}
                <li><div>Pinouts</div><div><ul className="pinoutsList">{pinouts}</ul></div></li>
                {datasheet}
            </Fragment>;
        }

        const screenSize = crtInfos.screenSize ? <li><span>Tube size</span><span>{crtInfos.screenSize} inches</span></li> : '';
        const heater = crtInfos.heaterVoltage ? <li><span>Heater</span><span>{crtInfos.heaterVoltage}v</span></li> : '';
        const G1 = crtInfos.G1 ? <li><span>G1</span><span>{crtInfos.G1}v</span></li> : '';
        const tubeListOpen = stateMonitorListOpen ? ' listOpen' : '';
        const monitorList = stateMonitorList.length ? <li><div>Tubes</div><div><button className={'tubeListBtn' + tubeListOpen} onClick={() => setstateMonitorListOpen(!stateMonitorListOpen)}>see compatible tubes (regardless yoke)</button><ul className={'tubeList' + tubeListOpen}>{renderTubeList()}</ul></div></li> : '';

        return (
            <ul className="ulTable">
                {screenSize}
                {crtSocket}
                {heater}
                {G1}
                {monitorList}
            </ul>
        )
    }


    //---------------------------//
    //---- RENDER TUBES LIST ----//
    //---------------------------//

    const renderTubeList = () => {

        return stateMonitorList.map((item, key) => {
            const brand = item.brand ? ' - ' + item.brand.brand : '';
            const flat = item.flat ? ' (flat)' : '';
            const infos = item.ref + brand + flat;

            return <li key={key}>{infos}</li>
        })
    }


    //---------------------//
    //---- MAIN RENDER ----//
    //---------------------//

    // if crt data exixt, display block
    if (crtInfos) {
        return (
            <div className="crtInfosBlock">
                <h2>CRT informations</h2>
                {renderList()}
            </div>
        )
    } else return null;
}

export default CrtInfosBlock;
