import React, { Fragment } from 'react'

const ComponentsBlock = ({ componentsInfos }) => {

    //---------------------//
    //---- RENDER LIST ----//
    //---------------------//

    const renderList = () => {
        const vot = componentsInfos.VOT ? <li><span>VOT</span><span>{componentsInfos.VOT}</span></li> : '';
        const deflec = componentsInfos.defelectionProc ? <li><span>Deflection<br />processor</span><span>{componentsInfos.defelectionProc}</span></li> : '';
        const str = componentsInfos.STR ? <li><span>STR</span><span>{componentsInfos.STR}</span></li> : '';
        const lightBulbTest = componentsInfos.THThotPin && componentsInfos.THTbplusPin ? <div className="lightBulbTest">Light bulb test points: {componentsInfos.THThotPin} (HOT) - {componentsInfos.THTbplusPin} (B+)</div> : '';

        return (
            <Fragment>
                <h2>Main components</h2>
                <ul className="ulTable">
                    <li>
                        <div>THT</div>
                        <div>{componentsInfos.THT}{lightBulbTest}</div>
                    </li>
                    <li>
                        <span>HOT</span>
                        <span>{componentsInfos.HOT}</span>
                    </li>
                    {vot}
                    {deflec}
                    {str}
                </ul>
            </Fragment>
        )
    }


    //---------------------------//
    //---- RENDER FUSES LIST ----//
    //---------------------------//

    const renderFusesList = () => {
        if (componentsInfos.fuses) {
            const list = componentsInfos.fuses.map((item, key) => {
                const FB = item.fastBlow ? <span>Fast Blow</span> : '';
                const SB = item.slowBlow ? <span>Slow Blow</span> : '';
                const size = item.size ? <span>({item.size})</span> : '';
                const role = item.role ? <span>- {item.role}</span> : '';

                return (
                    <li key={key}>
                        <div>{item.position}</div>
                        <div>{item.currentRating} amp {FB} {SB} {size} {role}</div>
                    </li>
                )
            });

            return (
                <Fragment>
                    <h2>Fuses</h2>
                    <ul className="ulTable">
                        {list}
                    </ul>
                </Fragment>
            )
        } else return null;
    }

    //---------------------//
    //---- MAIN RENDER ----//
    //---------------------//

    // if components data exixt, display block
    if (componentsInfos) {
        return (
            <div className="componentsBlock">
                {renderList()}
                {renderFusesList()}
            </div>
        )
    }
    else return null;
}

export default ComponentsBlock;
