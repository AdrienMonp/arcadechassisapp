import React from 'react'

const PotentiometersBlock = ({ potentiometersInfos }) => {


     //---------------------//
    //---- RENDER LIST ----//
    //---------------------//

    const renderList = () => {
        const potsGroupList = potentiometersInfos.map((item, key) => {

            // get pots infos
            let pots = item.potentiometers.map((pot, key) => {
                const position = pot.position ? pot.position : '';
                const value = pot.value ? `(${pot.value}Ω)` : '';
                const notes = pot.notes ? pot.notes.split('\n').map((item, key) => <div key={key}>{item}</div>) : '';

                return (
                    <li key={key}>
                        <div>{pot.role}</div>
                        <div>{position} {value} {notes}</div>
                    </li>
                )
            });

            // return list
            return (
                <li key={key}>
                    <div className="potsGroupTitle">{item.location}</div>
                    <ul className="ulTable">
                        {pots}
                    </ul>
                </li>
            )
        });

        return (
            <ul className="potsGroupList">
                {potsGroupList}
            </ul>
        )
    }

    //---------------------//
    //---- MAIN RENDER ----//
    //---------------------//

    if (potentiometersInfos.length) {
        return (
            <div className="potentiometersBlock">
                <h2>Potentiometers</h2>
                {renderList()}
            </div>
        )
    } else return null;
}

export default PotentiometersBlock;
