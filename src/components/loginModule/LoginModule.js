import React, { useState } from 'react';
import { APIurl } from '../common/common';
import { useSelector, useDispatch } from 'react-redux';
import { login, logout, openModale } from '../../actions';

const LoginModule = () => {

    // get info from global state
    const globalState = useSelector(state => state);
    const isLogged = globalState.login.logged;

    // dispatcher init
    const dispatch = useDispatch();


    //---------------------------------------------------//
    //---- HOOK STATES, EFFECTS AND REF DECLARATIONS ----//
    //---------------------------------------------------//

    // state for inputs value
    const [stateLogin, setstateLogin] = useState('');
    const [statePassword, setstatePassword] = useState('')

    // state for displaying loader when connecting
    const [stateConnecting, setstateConnecting] = useState(false);


    //------------------------//
    //---- LOGIN FUNCTION ----//
    //------------------------//

    const submitLogin = (e) => {
        e.preventDefault();
        setstateConnecting(true);
        let mounted = true;

        // send login and password to strapi
        fetch(`${APIurl}/auth/local`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json; charset=UTF-8' },
            body: JSON.stringify({
                identifier: stateLogin,
                password: statePassword
            })
        })
            .then(result => result.json())
            .then(json => {
                if (mounted) {
                    if (typeof json.error !== 'undefined') { // error catch
                        // display message error
                        const message = typeof json.message !== 'undefined' && json.message.length > 0 && typeof json.message[0].messages !== 'undefined' && json.message[0].messages.length > 0 && typeof json.message[0].messages[0].message !== 'undefined' ? json.message[0].messages[0].message : 'Login error';
                        dispatch(openModale(message));
                        // put login form back
                        setstateConnecting(false);
                    }
                    else { // call global login function
                        dispatch(login(json.jwt, json.user.username));
                        setstateConnecting(false);
                    }
                }
            });

        return function cleanup() {
            mounted = false;
        }
    }

    
    //-------------------------//
    //---- LOGOUT FUNCTION ----//
    //------------------------//

    const submitLogout = () => {
        dispatch(logout());
    }


    //------------------------------------//
    //---- STATE CHANGING WITH INPUTS ----//
    //------------------------------------//

    const changeLogin = (e) => {
        setstateLogin(e.target.value);
    }

    const changePassword = (e) => {
        setstatePassword(e.target.value);
    }

    
    //----------------------//
    //---- MAINR RENDER ----//
    //----------------------//

    if (isLogged === true) { // if logged display logout button
        return <button className="logoutBtn" onClick={() => submitLogout()}>Logout</button>
    } else {
        if (!stateConnecting) { // if not logged and not connecting display form
            return (
                <form className="loginForm" onSubmit={(e) => submitLogin(e)}>
                    <input className="loginInput" type="text" placeholder="login" onChange={(e) => changeLogin(e)} />
                    <input className="passworInput" type="password" placeholder="password" onChange={(e) => changePassword(e)} />
                    <input className="loginSubmitBtn" type="submit" value="Login" />
                </form>
            )
        } else { // else display loader
            return (
                <form className="loginForm">
                    <input className="loginInput" type="text" placeholder="login" defaultValue={stateLogin} disabled />
                    <input className="passworInput" type="password" placeholder="password" defaultValue={statePassword} disabled />
                    <div className="loginLoader"><div className="progressLoader"></div></div>
                </form>
            )
        }
    }
}

export default LoginModule;