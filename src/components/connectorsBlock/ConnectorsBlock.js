import React from 'react'

const ConnectorsBlock = ({ connectorsInfos }) => {

    //---------------------//
    //---- RENDER LIST ----//
    //---------------------//

    const renderList = () => {

        const items = connectorsInfos.map((item, key) => {
            const type = item.male ? 'male' : 'female';
            const reference = item.connector ? <li><span>Reference</span><span>{item.connector.name} {type}</span></li> : '';
            const position = item.position ? <li><span>Position</span><span>{item.position}</span></li> : '';
            const notes = item.notes ? <li><div>Notes</div><div>{item.notes.split('\n').map((item, key) => <div key={key}>{item}</div>)}</div></li> : '';

            let pinouts = '';
            if (item.pinouts.length) {
                pinouts = item.pinouts.map((pin, key) => {
                    return (
                        <li key={key}>
                            <span>{pin.pin}</span>
                            <span>{pin.role}</span>
                        </li>
                    )
                });
                pinouts = <li><div>Pinouts</div><div><ul className="pinoutsList">{pinouts}</ul></div></li>
            }
            

            return (
                <li key={key}>
                    <div className="connectorsListTitle">{item.role}</div>
                    <ul className="ulTable">
                        {reference}
                        {position}
                        {notes}
                        {pinouts}
                    </ul>
                </li>
            )
        });

        return (
            <ul className="connectorsList">
                {items}
            </ul>
        )
    }


    //---------------------//
    //---- MAIN RENDER ----//
    //---------------------//

    // if components data exixt, display block
    if (connectorsInfos) {
        return (
            <div className="connectorsBlock">
                <h2>Connectors</h2>
                {renderList()}
            </div>
        )
    }
    else return null;
}

export default ConnectorsBlock;