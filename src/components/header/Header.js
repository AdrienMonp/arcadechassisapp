import React from 'react';
import { Link } from "react-router-dom";
import LoginModule from '../loginModule/LoginModule';

const Header = () => {
    return (
        <div className="headerContainer">
            <Link className="backToHomeBtn" to={'/chassisList'}>
                THE ARCADE CHASSIS APP
            </Link>
            <LoginModule />
        </div>
    )
}

export default Header;