import React, { useRef, useState, useEffect, useLayoutEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { capaSpriteToCreate, setCapaList } from '../../actions';
import { APIurl } from '../common/common';

const CapaCanvas = ({ chassisId, capkitPhoto }) => {

    // API url suffix to target capas
    const chassisUrl = 'chassis-items';

    // get info from global state
    const globalState = useSelector(state => state);
    const globalCapaList = globalState.capaSprite.capaList;
    const capaCreationId = globalState.capaSprite.capaToCreate;
    const isLogged = globalState.login.logged;

    // dispatcher init
    const dispatch = useDispatch();


    //---------------------------------------------------//
    //---- HOOK STATES, EFFECTS AND REF DECLARATIONS ----//
    //---------------------------------------------------//

    // ref for canvas wrapper and canvas
    const canvasWrapperRef = useRef(null);
    const canvasRef = useRef(null);

    // width and height of canvas
    const [stateCanvasWidth, setstateCanvasWidth] = useState(0);
    const [stateCanvasHeight, setstateCanvasHeight] = useState(0);

    // coordinates of new capa
    const [stateNewCapaX, setstateNewCapaX] = useState(null);
    const [stateNewCapaY, setstateNewCapaY] = useState(null);
    const [stateNewCapaXb, setstateNewCapaXb] = useState(null);
    const [stateNewCapaYb, setstateNewCapaYb] = useState(null);

    //  id of dragged capa
    const [stateDraggedCapa, setstateDraggedCapa] = useState(null);

    // when mouse down and move to create a sprite
    const [stateSettingCapa, setstateSettingCapa] = useState(false);

    // edit mode on/off
    const [stateEditMode, setstateEditMode] = useState(false);

    // fullScreen
    const [stateFullScreen, setstateFullScreen] = useState(false);

    // when changing full screen mode, redifine state canvas size
    useEffect(() => {
        if (canvasWrapperRef.current) {
            setstateCanvasWidth(canvasWrapperRef.current.offsetWidth);
            setstateCanvasHeight(canvasWrapperRef.current.offsetHeight);
        }
    }, [stateFullScreen])


    //-------------------------------//
    //---- CANVAS SIZE FUNCTIONS ----//
    //-------------------------------//

    // update state canvas size when img loaded
    const ImgLoaded = () => {
        const canvasWrapper = canvasWrapperRef.current;
        setstateCanvasWidth(canvasWrapper.offsetWidth);
        setstateCanvasHeight(canvasWrapper.offsetHeight);
    }


    // update state canvas sise when window resized
    function UpdateCanvasSize() {

        useLayoutEffect(() => {
            function updateSize() {
                if (canvasWrapperRef.current) {
                    setstateCanvasWidth(canvasWrapperRef.current.offsetWidth);
                    setstateCanvasHeight(canvasWrapperRef.current.offsetHeight);
                }
            }
            window.addEventListener('resize', updateSize);
            updateSize();
            return () => window.removeEventListener('resize', updateSize);
        }, []);
    }
    UpdateCanvasSize();


    //------------------------------//
    //---- FULL SCREEN FUNCTION ----//
    //------------------------------//

    const fullScreen = () => {
        if (!stateFullScreen) { // add/remove noscroll class on body
            document.body.classList.add('bodyNoScroll');
        } else {
            document.body.classList.remove('bodyNoScroll');
        }
        // update state
        setstateFullScreen(stateFullScreen ? false : true);
    }


    //---------------------------------//
    //---- DRAGGING CAPA FUNCTIONS ----//
    //---------------------------------//

    // mouse down to drag capa
    const mouseDownDrag = (e) => {
        // save id of selected capa
        const eltId = e.target.id;
        setstateDraggedCapa(eltId);

        // change mouse cursor
        if (eltId) {
            document.getElementById(eltId).style.cursor = 'grabbing';
        }
    }


    // moving capa
    const mouseMoveDrag = (e) => {
        // if a capa is dragged
        if (stateDraggedCapa) {
            moveCapa(e);
        }
    }


    // mouse up to drag capa
    const mouseUpDrag = (e) => {

        // if a capa has been dragged
        if (stateDraggedCapa) {
            moveCapa(e, true);
        }

        // reset state of draggable item
        setstateDraggedCapa(null);
    }


    // calculate coordinates when moving
    const moveCapa = (e, save = false) => {
        const canvasWrapper = canvasWrapperRef.current;
        const capa = document.getElementById(stateDraggedCapa);
        const pageY = stateFullScreen ? e.clientY : e.pageY; // use different props if fullscreen because no scroll
        // calculate new position
        const capaHalfWidth = capa.offsetWidth / 2;
        const capaHalfHeight = capa.offsetHeight / 2;
        const newX = ((e.pageX - canvasWrapper.offsetLeft) - capaHalfWidth) / stateCanvasWidth * 100;
        const newY = ((pageY - canvasWrapper.offsetTop) - capaHalfHeight) / stateCanvasHeight * 100;
        // set new position
        capa.style.left = newX + '%';
        capa.style.top = newY + '%';

        // if mouse up, save infos into global state
        if (save) {
            // change mouse cursor
            capa.style.cursor = '';
            // find id
            const id = parseInt(stateDraggedCapa.replace('capaSprite_', ''));
            // find index of capa
            const indexOfCapa = globalCapaList.findIndex(i => i.id === id);
            // if out of canvas, delete sprite
            let newObj = {};
            newObj.sprite = globalCapaList[indexOfCapa].sprite;
            // check if capa is still inside canvas
            if (newX < 0 || newX + newObj.sprite.width > 100 || newY < 0 || newY + newObj.sprite.height > 100) {
                newObj.sprite = null;
            } else {
                newObj.sprite.left = newX;
                newObj.sprite.top = newY;
            }
            // update API infos with new capa positions
            updateAPIcapa(newObj, indexOfCapa, id);
        }
    }


    //-------------------------------------//
    //---- NEW CAPA CREATION FUNCTIONS ----//
    //-------------------------------------//

    // mouse down to create capa
    const mouseDownCreate = (e) => {
        const canvasWrapper = canvasWrapperRef.current;
        const pageY = stateFullScreen ? e.clientY : e.pageY; // use different props if fullscreen because no scroll
        setstateNewCapaX(e.pageX - canvasWrapper.offsetLeft);
        setstateNewCapaY(pageY - canvasWrapper.offsetTop);
        setstateSettingCapa(true);
    }


    // moving mouse while creating capa
    const mouseMoveCreate = (e) => {

        if (stateSettingCapa) {
            const canvasWrapper = canvasWrapperRef.current;
            const capa = document.getElementById('capaCanvasFakeSprite');
            const pageY = stateFullScreen ? e.clientY : e.pageY; // use different props if fullscreen because no scroll
            const props = calculateCircleProps((e.pageX - canvasWrapper.offsetLeft), (pageY - canvasWrapper.offsetTop))

            // set new position
            capa.style.left = props.left + '%';
            capa.style.top = props.top + '%';
            capa.style.width = props.width + '%';
            capa.style.height = props.height + '%';
        }
    }


    // mouse up to create capa
    const mouseUpCreate = (e) => {
        const canvasWrapper = canvasWrapperRef.current;
        const capa = document.getElementById('capaCanvasFakeSprite');
        const pageY = stateFullScreen ? e.clientY : e.pageY; // use different props if fullscreen because no scroll
        setstateNewCapaXb(e.pageX - canvasWrapper.offsetLeft);
        setstateNewCapaYb(pageY - canvasWrapper.offsetTop);
    
        // clear fake capa
        capa.style.left = 0;
        capa.style.top = 0;
        capa.style.width = 0;
        capa.style.heigt = 0;
        setstateSettingCapa(false);
    }


    // calculate and save new capa infos
    const saveCapaInfos = () => {

        if (capaCreationId) {
            // find index of capa
            const indexOfCapa = globalCapaList.findIndex(i => i.id === capaCreationId);
            // create obj with new sprite values
            const newObj = { sprite: calculateCircleProps(stateNewCapaXb, stateNewCapaYb) };
            // add angle
            const angle = ((Math.atan2(stateNewCapaY - stateNewCapaYb, stateNewCapaX - stateNewCapaXb) * 180 / Math.PI) + 180) % 360;
            newObj.sprite.angle = angle;

            // if capa not outside of canvas, update API infos with new capa obj
            const sprite = newObj.sprite;
            if (sprite.width > 0 && sprite.height > 0 && sprite.top > 0 && sprite.top + sprite.height < 100 && sprite.left > 0 && sprite.left + sprite.width < 100) {
                updateAPIcapa(newObj, indexOfCapa, capaCreationId);
            }
        }
    }

    // when last new coordinate updated, save all infos
    useEffect(saveCapaInfos, [stateNewCapaYb]);


    // calculate circle width, height, left and top
    const calculateCircleProps = (currentX, currentY) => {
        const X = Math.abs(stateNewCapaX - currentX);
        const Y = Math.abs(stateNewCapaY - currentY);
        const halfWidth = Math.sqrt(X * X + Y * Y);
        const halfHeight = Math.sqrt(X * X + Y * Y);
        const width = halfWidth * 2 / stateCanvasWidth * 100;
        const height = halfHeight * 2 / stateCanvasHeight * 100;
        const top = ((stateNewCapaY - halfWidth) / stateCanvasHeight) * 100;
        const left = ((stateNewCapaX - halfWidth) / stateCanvasWidth) * 100;

        return { width, height, left, top };
    }


    //-----------------------------//
    //---- UPDATE API FUNCTION ----//
    //-----------------------------//

    const updateAPIcapa = (newObj, indexOfCapa, id) => {

        // send infos to update to API
        fetch(`${APIurl}/${chassisUrl}/updatecapa/${chassisId}/${id}`, {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json; charset=UTF-8', 'Authorization': `Bearer ${globalState.login.key}` },
            body: JSON.stringify(newObj)
        })
            .then(result => result.json())
            .then(json => {
                if (typeof json.error !== 'undefined') { // error catch
                    console.log(`Error ${json.statusCode} : ${json.error} - ${json.message}`);
                } else {
                    const newCapaInfos = [...globalCapaList];
                    newCapaInfos[indexOfCapa] = json;
                    dispatch(setCapaList(newCapaInfos));
                }
                // reset capa to create global state
                dispatch(capaSpriteToCreate(null));
            });
    }


    //---------------------------------//
    //---- RENDER ALL CAPA SPRITES ----//
    //---------------------------------//

    const renderCapas = (capas) => {

        return capas.map((item, key) => {
            const values = item.sprite;

            if (values) {
                // if bipolar hide orientation
                const bipolar = item.compId.charAt(12) === '1' ? 'BP' : '';

                const styles = {
                    width: values.width + '%',
                    height: values.height + '%',
                    top: values.top + '%',
                    left: values.left + '%',
                    transform: `rotate(${values.angle}deg)`
                }

                return (
                    <div id={'capaSprite_' + item.id} className={bipolar} key={key} style={styles}>
                        <span>{item.position}</span>
                    </div>
                )
            } else {
                return null
            }
        });
    }


    //-----------------------//
    //---- RENDER CANVAS ----//
    //-----------------------//

    const renderCanvas = () => {

        // photo check
        if (capkitPhoto) {

            // edit button
            let editButton = '';
            if (isLogged) {
                const editBtn = stateEditMode ? ' exitMode' : '';
                const editBtnText = stateEditMode ? 'Quit' : 'Edit';
                editButton = <button className={'editBtn' + editBtn} onClick={() => setstateEditMode(!stateEditMode)}>{editBtnText}</button>;
            }

            // full screen management

            const exit = stateFullScreen ? 'Exit' : '';

            // class and events depend on current mode
            const opts = {};
            if (capaCreationId) { // create mode
                opts['className'] = 'capaCanvas creationMode';
                opts['onMouseDown'] = (e) => mouseDownCreate(e);
                opts['onMouseUp'] = (e) => mouseUpCreate(e);
                opts['onMouseMove'] = (e) => mouseMoveCreate(e);
            } else if (stateEditMode) { // edit mode
                opts['className'] = 'capaCanvas editMode';
                opts['onMouseDown'] = (e) => mouseDownDrag(e);
                opts['onMouseUp'] = (e) => mouseUpDrag(e);
                opts['onMouseMove'] = (e) => mouseMoveDrag(e);
            } else {
                opts['className'] = 'capaCanvas';
            }

            // return canvas
            return (
                <div ref={canvasWrapperRef} className="capaCanvasWrapper" >
                    <img src={APIurl + capkitPhoto.url} alt="" onLoad={() => ImgLoaded()} />
                    <button className="canvasFullScreenBtn" onClick={() => fullScreen()}>
                        <img src={`${process.env.PUBLIC_URL}/images/icons/fullScreen${exit}.svg`} alt="Enter/exit full screen" />
                    </button>
                    {editButton}
                    <div ref={canvasRef} {...opts}>
                        <span id="capaCanvasFakeSprite"></span>
                        {renderCapas(globalCapaList)}
                    </div>
                </div>
            );
        }
        else { // no photo exists
            return <span>No photo</span>
        }
    }

    //---------------------//
    //---- MAIN RENDER ----//
    //---------------------//

    const isFullScreen = stateFullScreen ? ' fullScreen' : '';
    return (
        <div className={'capaCanvasContainer' + isFullScreen}>
            {renderCanvas()}
        </div >
    )
}

export default CapaCanvas;
