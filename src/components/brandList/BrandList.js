import React, { useState, useEffect } from 'react'
import { Link } from "react-router-dom";
import { APIurl } from '../common/common';

const BrandList = () => {

    // API url suffix to target brand list
    const urlSuffix = 'manufacturers';


    //---------------------------------------------------//
    //---- HOOK STATES, EFFECTS AND REF DECLARATIONS ----//
    //---------------------------------------------------//

    // set state - brands values
    const [stateBrands, setstateBrands] = useState([]);

    // after first painting, get brands from API
    useEffect(() => {
        getBrandsFromAPI();
    }, []);


    //-----------------------------//
    //---- GET BRANDS FROM API ----//
    //-----------------------------//

    const getBrandsFromAPI = async () => {

        await fetch(`${APIurl}/${urlSuffix}`)
            .then(result => result.json())
            .then(json => {
                if (typeof json.error !== 'undefined') { // error catch
                    console.log(`Error ${json.statusCode} : ${json.error} - ${json.message}`);
                } else {
                    // save list into state
                    setstateBrands(json);
                }
            });
    };


    //---------------------------//
    //---- RENDER BRAND LIST ----//
    //---------------------------//

    const RenderList = () => {

        return stateBrands.map((item, key) => {

            return (
                <li key={key}>
                    <h2>{item.name}</h2>
                    <ul className="chassisSubList">
                        {RenderSubList(item.chassisList)}
                    </ul>
                </li>
            )
        });
    }


    //-----------------------------//
    //---- RENDER CHASSIS LIST ----//
    //-----------------------------//

    const RenderSubList = (links) => {

        return links.map((item, key) => {

            return (
                <li key={key}><Link to={`/chassis/${item.url}`}>{item.ref}</Link></li>
            )
        })
    }


    //---------------------//
    //---- MAIN RENDER ----//
    //---------------------//

    return (
        <ul className="chassisList">
            <RenderList />
        </ul>
    )
}

export default BrandList;