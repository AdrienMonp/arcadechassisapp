import React, { Fragment, useEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { closeModale } from '../../actions';

const Modale = () => {

    // get info from global state
    const globalState = useSelector(state => state);
    const modaleOpened = globalState.modale.displayed;
    const modaleContent = globalState.modale.content;

    // dispatcher init
    const dispatch = useDispatch();

    
    // bind closeModale to escape key
    const escapeKeyPressed = useCallback((e) => {
        if (modaleOpened && e.keyCode === 27) {
            dispatch(closeModale());
        }
    }, [modaleOpened, dispatch]);

    useEffect(() => {
        document.addEventListener("keydown", escapeKeyPressed, false);

        return () => {
            document.removeEventListener("keydown", escapeKeyPressed, false);
        };
    }, [escapeKeyPressed]);


    // check if modale is displayed or not
    const display = modaleOpened ? '' : 'hidden';

    return (
        <Fragment>
            <div className={`modaleOverlay ${display}`}></div>
            <div className={`modaleContainer ${display}`} >
                <div className="modaleWrapper">
                    <button className="modaleCloseBtn" onClick={() => dispatch(closeModale())}>x</button>
                    {modaleContent}
                </div>
            </div>
        </Fragment>
    )
}

export default Modale;
