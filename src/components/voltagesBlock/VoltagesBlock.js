import React, { useState } from 'react';
import { APIurl } from '../common/common';

const VoltagesBlock = ({ voltagesInfos }) => {

    //---------------------------------------------------//
    //---- HOOK STATES, EFFECTS AND REF DECLARATIONS ----//
    //---------------------------------------------------//

    // full screen state
    const [stateFullScreen, setstateFullScreen] = useState(false);


    //------------------------------//
    //---- FULL SCREEN FUNCTION ----//
    //------------------------------//

    const fullScreen = () => {
        if (!stateFullScreen) { // add/remove noscroll class on body
            document.body.classList.add('bodyNoScroll');
        } else {
            document.body.classList.remove('bodyNoScroll');
        }
        setstateFullScreen(!stateFullScreen);
    }


    //---------------------//
    //---- RENDER LIST ----//
    //---------------------//

    const renderList = () => {

        // b+ test points photo row
        let bPlusPhoto = '';
        if (voltagesInfos.bPlusTestPhoto) {
            const bPlusPhotoUrl = stateFullScreen ? APIurl + voltagesInfos.bPlusTestPhoto.url : APIurl + voltagesInfos.bPlusTestPhoto.formats.thumbnail.url;
            const fullScreenClass = stateFullScreen ? ' fullScreen' : '';
            bPlusPhoto = <li><span>Test points</span>
                <div className={'bPlusPhotoContainer' + fullScreenClass} onClick={() => fullScreen()}>
                    <img src={bPlusPhotoUrl} alt="b+ test points" />
                    <button className="bPlusFullScreenExitBtn">Exit</button>
                </div></li>;
        }
        // b+ notes row
        const bPlusNotes = voltagesInfos.bPlusNotes ? <li><span>Notes</span><span>{voltagesInfos.bPlusNotes.split('\n').map((item, key) => <div key={key}>{item}</div>)}</span></li> : '';
        // second anode row
        const secondeAnodeNote = voltagesInfos.secondAnodeNote ? ` (${voltagesInfos.secondAnodeNote})` : '';

        return (
            <ul className="ulTable">
                <li>
                    <span>B+ voltage</span>
                    <span>{voltagesInfos.bPlusVoltage} v</span>
                </li>
                <li>
                    <span>Ground test point</span>
                    <span>{voltagesInfos.bPlusGroundPoint}</span>
                </li>
                <li>
                    <span>Positive test point</span>
                    <span>{voltagesInfos.bPlusPositivePoint}</span>
                </li>
                {bPlusPhoto}
                <li>
                    <span>B+ potentiometer</span>
                    <span>{voltagesInfos.bPlusPotentiometer}</span>
                </li>
                {bPlusNotes}
                <li>
                    <span>Seconde anode</span>
                    <span>{voltagesInfos.secondAnodeVoltage} Kv {secondeAnodeNote}</span>
                </li>
            </ul>
        )
    }


    //---------------------//
    //---- MAIN RENDER ----//
    //---------------------//

    // if voltages data exist, display block
    if (voltagesInfos) {
        return (
            <div className="voltagesBlock">
                <h2>Voltages</h2>
                {renderList()}
            </div>
        )
    } else return null;
}

export default VoltagesBlock;
