import React, { useState, useEffect } from 'react'
import { APIurl } from '../common/common';

const Carousel = ({ chassisId, chassisPhotos }) => {

    //---------------------------------------------------//
    //---- HOOK STATES, EFFECTS AND REF DECLARATIONS ----//
    //---------------------------------------------------//

    const [stateImageList, setStateImageList] = useState([]);
    const [stateIndex, setStateIndex] = useState(0);
    const [stateNumberOfItems, setStateNumberOfItems] = useState(0);
    const [stateFullScreen, setStateFullScreen] = useState(false);


    // when chassisPhotos paramater change, re-render
    useEffect(() => {
        // set photo list into state
        setStateImageList([...chassisPhotos]);
        // save number of photos
        setStateNumberOfItems(chassisPhotos.length);
    }, [chassisPhotos]);


    //---------------------------------------//
    //---- GO TO PREV AND NEXT FUNCTIONS ----//
    //---------------------------------------//

    const goToPrev = () => {
        setStateIndex(stateIndex === 0 ? stateNumberOfItems - 1 : stateIndex - 1);
    }

    const goToNext = () => {
        setStateIndex(stateIndex === stateNumberOfItems - 1 ? 0 : stateIndex + 1);
    }


    //------------------------------//
    //---- FULL SCREEN FUNCTION ----//
    //------------------------------//

    const fullScreen = () => {
        if (!stateFullScreen) { // add/remove noscroll class on body
            document.body.classList.add('bodyNoScroll');
        } else {
            document.body.classList.remove('bodyNoScroll');
        }
        // update state
        setStateFullScreen(stateFullScreen ? false : true);
    }


    //-----------------------------------//
    //---- RENDER CAROUSEL CONTAINER ----//
    //-----------------------------------//

    const CarouselContainer = () => {

        // full screen management
        const isFullScreen = stateFullScreen ? ' fullScreen' : '';
        const exit = stateFullScreen ? 'Exit' : '';

        return (
            <div className="carouselContainer">
                <div className={'carouselWrapper' + isFullScreen} >
                    <button className="carouselFullScreenBtn" onClick={() => fullScreen()}>
                        <img src={`${process.env.PUBLIC_URL}/images/icons/fullScreen${exit}.svg`} alt="Enter/exit full screen" />
                    </button>
                    <button className="carouselArrow prevArrow" onClick={() => goToPrev()}>
                        <img src={`${process.env.PUBLIC_URL}/images/icons/arrow-right.svg`} alt="previous" />
                    </button>
                    <div className="carouselDraggable">
                        <CarouselItems />
                    </div>
                    <button className="carouselArrow nextArrow" onClick={() => goToNext()}>
                        <img src={`${process.env.PUBLIC_URL}/images/icons/arrow-right.svg`} alt="next" />
                    </button>
                </div>
            </div>
        );
    }


    //-------------------------------//
    //---- RENDER CAROUSEL ITEMS ----//
    //-------------------------------//

    const CarouselItems = () => {
        const sizingType = stateFullScreen ? 'contain' : 'cover';

        return stateImageList.map((item, key) => {

            // check if photo exists
            if (typeof item.photo !== 'undefined') {
                const url = stateFullScreen ? APIurl + item.photo.url : APIurl + item.photo.formats.large.url;

                const styles = {
                    backgroundImage: `url(${url})`,
                    backgroundSize: sizingType,
                    backgroundPosition: 'center',
                    backgroundRepeat: 'no-repeat'
                };

                // if current slide, display it
                const classModifier = key === stateIndex ? ' current' : '';

                // if notes exists, insert it
                const note = item.legend !== '' ? <div className='carouselNotes'>{item.legend}</div> : ''

                return (
                    <div key={key} className={'carouselItem' + classModifier} style={styles}>{note}</div>
                )
            } else {
                return null;
            }
        });
    }


    //---------------------//
    //---- MAIN RENDER ----//
    //---------------------//

    return (
        <CarouselContainer />
    )
}

export default Carousel;