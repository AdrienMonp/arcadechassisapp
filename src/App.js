import React from 'react';
import ChassisListPage from './pages/chassisListPage/ChassisListPage'

function App() {
  return (
    <div className="App">
      <ChassisListPage/>
    </div>
  );
}

export default App;
