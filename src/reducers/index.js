import { LOGIN, LOGOUT, OPENMODALE, CLOSEMODALE, SETCAPALIST, CAPASPRITETOCREATE } from "../actions/types";


let initialState = {
    login: {
        logged: false,
        name: '',
        key: ''
    },
    modale: {
        displayed: false,
        content: ''
    },
    capaSprite: {
        capaToCreate: null,
        capaList: []
    }
}

// check session storage to get infos
if (sessionStorage.getItem('chassisApp') !== null) {
    const infosString = sessionStorage.getItem('chassisApp');
    initialState.login = JSON.parse(infosString);
}


export default function rootReducer(state = initialState, action) {

    let newState = { ...initialState };

    switch (action.type) {

        case LOGIN:
            // set new state
            newState.login.logged = true;
            newState.login.name = action.payload.username;
            newState.login.key = action.payload.key

            // save it to session storage
            sessionStorage.setItem('chassisApp', JSON.stringify({ ...newState.login }));

            return newState


        case LOGOUT:
            // set new state
            newState.login.logged = false;
            newState.login.name = '';
            newState.login.key = ''

            // save it to session storage
            sessionStorage.setItem('chassisApp', JSON.stringify({ ...newState.login }));

            return newState


        case OPENMODALE:
            // update content
            newState.modale.content = action.payload.content;
            // update displayed state
            newState.modale.displayed = true;
            // fix scroll
            document.body.classList.add('bodyNoScroll');

            return newState


        case CLOSEMODALE:
            // empty content
            newState.modale.content = '';
            // update displayed state
            newState.modale.displayed = false;
            // release scroll
            document.body.classList.remove('bodyNoScroll');

            return newState


        case SETCAPALIST:
            newState.capaSprite.capaList = action.payload.list;

            return newState


        case CAPASPRITETOCREATE:
            newState.capaSprite.capaToCreate = action.payload.id;

            return newState


        default:
            return state;
    }

}