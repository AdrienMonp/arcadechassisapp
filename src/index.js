import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from './reducers';
import App from './App';
import ChassisListPage from './pages/chassisListPage/ChassisListPage';
import ChassisPage from './pages/chassisPage/ChassisPage';
import NotFoundPage from './pages/notFoundPage/NotFoundPage';
import * as serviceWorker from './serviceWorker';

import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom';


// Création du store via Redux
const store = createStore(
    rootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);


const Root = () => (
    <Router>
        <Switch>
            <Route exact path='/' component={App} />
            <Route exact path='/chassisList' component={ChassisListPage} />
            <Route path='/chassis/:id' component={ChassisPage} />
            <Route component={NotFoundPage} />
        </Switch>
    </Router>
);


ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <Root />
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
