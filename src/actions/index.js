import { LOGIN, LOGOUT, OPENMODALE, CLOSEMODALE, SETCAPALIST, CAPASPRITETOCREATE } from './types';

export const login = (key, username) => {
    return({
        type: LOGIN,
        payload: {
            key,
            username
        }
    });
};

export const logout = () => {
    return({
        type: LOGOUT,
        payload: {
        }
    });
};

export const openModale = (content) => {
    return({
        type: OPENMODALE,
        payload: {
            content
        }
    });
}

export const closeModale = () => {
    return({
        type: CLOSEMODALE,
        payload: {
        }
    });
}

export const setCapaList = (list) => {
    return({
        type: SETCAPALIST,
        payload: {
            list
        }
    });
}

export const capaSpriteToCreate = (id) => {
    return({
        type: CAPASPRITETOCREATE,
        payload: {
            id
        }
    });
}
