// On définit des types pour indiquer ce que va faire l'action
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const OPENMODALE = 'OPENMODALE';
export const CLOSEMODALE = 'CLOSEMODALE';
export const SETCAPALIST = 'SETCAPALIST';
export const CAPASPRITETOCREATE = 'CAPASPRITETOCREATE';