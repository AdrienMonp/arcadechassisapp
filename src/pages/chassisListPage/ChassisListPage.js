import React from 'react';
import BrandList from '../../components/brandList/BrandList';
import Header from '../../components/header/Header';
import Modale from '../../components/modale/Modale';

export const ChassisListPage = () => {

    return (
        <div className="appMainContainer">
            <Header />
            <h1 className="chassisListTitle">Chassis by brands</h1>
            <div className="chassisListContainer">
                <BrandList />
            </div>
            <Modale />
        </div>
    )
}

export default ChassisListPage;