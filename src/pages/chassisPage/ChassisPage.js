import React, { useState, useEffect, useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { setCapaList } from '../../actions';
import Carousel from '../../components/carousel/Carousel';
import ChassisInfos from '../../components/chassisInfos/ChassisInfos';
import CapkitList from '../../components/capkit/CapkitList';
import { APIurl } from '../../components/common/common';
import Header from '../../components/header/Header';
import Modale from '../../components/modale/Modale';
import CapaCanvas from '../../components/capaCanvas/CapaCanvas';
import VoltagesBlock from '../../components/voltagesBlock/VoltagesBlock';
import ComponentsBlock from '../../components/componentsBlock/ComponentsBlock';
import CrtInfosBlock from '../../components/crtInfosBlock/CrtInfosBlock';
import ConnectorsBlock from '../../components/connectorsBlock/ConnectorsBlock';
import PotentiometersBlock from '../../components/potentiometersBlock/PotentiometersBlock';

const ChassisPage = (props) => {

    // get the current chassis Id into the url
    const chassisId = props.match.params.id;

    // API url suffix to target infos list
    const dataUrl = 'chassis-items';

    // dispatcher init
    const dispatch = useDispatch();


    //---------------------------------------------------//
    //---- HOOK STATES, EFFECTS AND REF DECLARATIONS ----//
    //---------------------------------------------------//

    // set state - infos values
    const [stateAPIinfos, setstateAPIinfos] = useState({});

    // set state - title of the page
    const [stateTitle, setstateTitle] = useState('');

    // state chassis photos
    const [stateAPIphotos, setstateAPIphotos] = useState([]);

    // state capkit photo
    const [stateAPIcapkitPhoto, setstateAPIcapkitPhoto] = useState(null);

    // state voltages infos
    const [stateVoltagesInfos, setstateVoltagesInfos] = useState(null);

    // state components infos
    const [stateMainComponentsInfos, setstateMainComponentsInfos] = useState(null);

    // state CRT infos
    const [stateCRTinfos, setstateCRTinfos] = useState(null);

    // state connectors infos
    const [stateConnectorsInfos, setstateConnectorsInfos] = useState(null);

    // state potentiometers infos
    const [statePotentiometersInfos, setstatePotentiometersInfos] = useState([]);


    //----------------------------------------//
    //---- GET CHASSIS ALL INFOS FROM API ----//
    //----------------------------------------//

    // memoize infos from API
    const getChassisDataFromAPI = useCallback(async () => {

        await fetch(`${APIurl}/${dataUrl}/${chassisId}`)
            .then(result => result.json())
            .then(json => {
                if (typeof json.error !== 'undefined') { // error catch
                    console.log(`Error ${json.statusCode} : ${json.error} - ${json.message}`);
                } else {
                    // update page title
                    setstateTitle(`${json.manufacturer.name} ${json.ref}`);
                    // save chassis photos for carousel
                    if (typeof json.carousel[0] !== 'undefined') {
                        setstateAPIphotos(json.carousel);
                    }
                    // save capkit photo for canvas
                    if (json.photoCapkit) {
                        setstateAPIcapkitPhoto(json.photoCapkit);
                    }
                    // save infos
                    setstateAPIinfos(json);
                    // save components
                    if (json.mainComponents) {
                        setstateMainComponentsInfos(json.mainComponents);
                    }
                    // save CRT infos
                    let crtInfos = {};
                    if (json.crtInfos) {
                        crtInfos = json.crtInfos;
                    }
                    if (json.crtSocket) {
                        crtInfos.crtSocket = json.crtSocket;
                    }
                    if (Object.keys(crtInfos).length) {
                        setstateCRTinfos(crtInfos);
                    }
                    // save voltages
                    if (json.voltages) {
                        setstateVoltagesInfos(json.voltages);
                    }
                    // save connectors
                    if (json.connectors) {
                        setstateConnectorsInfos(json.connectors);
                    }
                    // save potentiometers
                    if (json.potentiometers) {
                        setstatePotentiometersInfos(json.potentiometers);
                    }
                    // save capas
                    if (typeof json.capkit[0] !== 'undefined') {
                        dispatch(setCapaList(json.capkit));
                    } else {
                        dispatch(setCapaList([]));
                    }
                }
            });
    }, [chassisId, dispatch]);


    // after first painting, get infos and capas from API
    useEffect(() => {
        getChassisDataFromAPI();
    }, [getChassisDataFromAPI]);


    //---------------------//
    //---- MAIN RENDER ----//
    //---------------------//

    return (
        <div className="appMainContainer">
            <Header />
            <div className="chassisPageContainer">
                <h1 className="chassisPageTitle">{stateTitle}</h1>
                <div className="carouselAndInfosContainer">
                    <Carousel
                        chassisId={chassisId}
                        chassisPhotos={stateAPIphotos}
                    />
                    <ChassisInfos
                        chassisId={chassisId}
                        chassisInfos={stateAPIinfos}
                    />
                </div>
                <h2>Technical informations</h2>
                <div className="technicalInfosBlocksContainer">
                    <div className="voltagesBlockContainer">
                        <VoltagesBlock
                            voltagesInfos={stateVoltagesInfos}
                        />
                    </div>
                    <div className="componentsBlockContainer">
                        <ComponentsBlock
                            componentsInfos={stateMainComponentsInfos}
                        />
                    </div>
                    <div className="crtInfosBlockContainer">
                        <CrtInfosBlock
                            crtInfos={stateCRTinfos}
                        />
                    </div>
                </div>
                <div className="connectorsBlockContainer">
                    <ConnectorsBlock
                        connectorsInfos={stateConnectorsInfos}
                    />
                </div>
                <div className="potentiometersBlockContainer">
                    <PotentiometersBlock
                        potentiometersInfos={statePotentiometersInfos}
                    />
                </div>
                <h2>Aluminium electrolytic capacitors</h2>
                <div className="canvasAndCapkitListContainer">
                    <CapaCanvas
                        chassisId={chassisId}
                        capkitPhoto={stateAPIcapkitPhoto}
                    />
                    <CapkitList
                        chassisId={chassisId}
                        capkitPhoto={stateAPIcapkitPhoto}
                    />
                </div>
            </div>
            <Modale />
        </div>
    )
}

export default ChassisPage;
